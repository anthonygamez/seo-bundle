<?php

namespace Microblau\SeoBundle\Twig;

use Twig_SimpleFunction;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\Core\Helper\TranslationHelper;

use Twig_Environment as Environment;


class SeoExtension extends \Twig_Extension
{
    private $resolver;

    private $twig;

    private $cs;

    private $translator;

    private $defaultTagsConfig;

    private $contentDefaultId;

    private $contentDefault = null;

    private $tagList;

    private $completeList = array();

    private $defaults;

    private $social_sharing;

    private $fieldsMapping;

    public function __construct(ConfigResolverInterface $configResolver, Environment $twig, ContentService $cs , TranslationHelper $translator)
    {
        
        $this->resolver = $configResolver;
        $this->twig = $twig;
        $this->cs = $cs;
        $this->translator = $translator;

        $this->defaultTagsConfig = $this->resolver->getParameter('defaults', 'microblau', 'seo');
        $this->tagList = $this->defaultTagsConfig['tag_list'];
        $this->contentDefaultId = $this->defaultTagsConfig['contentDefaultId'];

        // Busca els camps a la config
        $seoConfig = $this->resolver->getParameter('config', 'microblau', 'seo');
        if($seoConfig){
            if (array_key_exists('social_sharing', $seoConfig)) {
                $this->socialSharing = $seoConfig['social_sharing'];
            }
            if (array_key_exists('field_mapping', $seoConfig)) {
                $this->fieldsMapping = $seoConfig['field_mapping'];
            }
        }
    }

    public function getFilters(){

        return array(
            new \Twig_SimpleFilter('remove_special', array($this, 'remove_special')),
        );        
        
    }
    
    public function remove_special($string){
        $string = strtolower($string);
        $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
        $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");

  return str_replace($search, $replace, $string);
        
        return $string;
       
    }
    
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('seo', array($this, 'seo')),
            new \Twig_SimpleFunction('seoValues', array($this, 'seoValues')),
            new \Twig_SimpleFunction('overwriteWithXrow', array($this, 'overwriteWithXrow')),
        );
    }

    public function seo($location = null, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        if ($location != null) {

            $content = $this->cs->loadContent($location->contentInfo->id);
            $this->getFieldValues($this->fieldsMapping, $content);
            return $this->twig->display('MicroblauSeoBundle::seo.html.twig', array('tagList' => $this->completeList));

        }
        return null;
    }
    public function seoValues($location = null, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        if ($location != null) {

            $content = $this->cs->loadContent($location->contentInfo->id);
            $this->getFieldValues($this->fieldsMapping, $content);
            return $this->completeList;

        }

        return null;
    }

    public function overwriteWithXrow($seoValues = null, $xrowFieldValues = null)
    {
        foreach($xrowFieldValues as $key => $value){
            if( $value != '' ){
                $seoValues[$key] = $value;
            }
        }

        return $this->twig->display('MicroblauSeoBundle::seo.html.twig', array('tagList' => $seoValues));
    }

    public function getFieldValues($mapping, $content)
    {

        $contentTypeId = $content->contentInfo->contentTypeId;
        if($mapping){
            if (array_key_exists($contentTypeId, $mapping)) {
                foreach ($mapping[$contentTypeId] as $tagId => $mappedField) {
                    if ($content->getFieldValue($mappedField) != null) {
                        $this->completeList[$tagId] = $this->translator->getTranslatedField($content, $mappedField)->value;
                    }
                }
            }
        }

        foreach ($this->tagList as $tagId => $value) {
            if (!array_key_exists($tagId, $this->completeList)) {
                $this->getDefaultTag($content, $tagId, $value);
            }
        }
    }

    public function getDefaultTag($content, $tagId, $value)
    {
        if (is_null($this->contentDefault))
        {
            $this->contentDefault = $this->cs->loadContent($this->contentDefaultId);
        }
        
        if ( isset($this->fieldsMapping['defaults'][$tagId]) )
        {
            $defaultFieldMapped = $this->fieldsMapping['defaults'][$tagId];
            if ($content->getFieldValue($defaultFieldMapped) && !isset($content->getFieldValue($defaultFieldMapped)->xml))
            {
                $this->completeList[$tagId] = $this->translator->getTranslatedField($content, $defaultFieldMapped)->value;
                return;
            }
            else if ($this->contentDefault->getFieldValue($defaultFieldMapped) && !isset($this->contentDefault->getFieldValue($defaultFieldMapped)->xml))
            {
                $this->completeList[$tagId] = $this->translator->getTranslatedField( $this->contentDefault, $defaultFieldMapped)->value;
                return;
            }


        }
        // Set fallback value from default list
        $this->completeList[$tagId] = $value;
    }

    public function getName()
    {
        return 'seo_extension';
    }
}
